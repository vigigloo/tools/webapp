# Generic web app Helm Chart

This is a helm chart designed to deploy any typical web app on your Kubernetes cluster. Our aim is to handle generic use-cases and provide a flexible chart to cater to unique requirements.

## Prerequisites

Should be working with older versions, but tested from these ones

- Kubernetes 1.26+
- Helm 3.12+

## Parameters

### Global parameters

| Name               | Description                                                                                                                                                    | Value |
| ------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----- |
| `replicaCount`     | represents the number of pods that will be created for the application                                                                                         | `1`   |
| `nameOverride`     | represents the name that should override the default naming of the application                                                                                 | `""`  |
| `fullnameOverride` | represents the full name that should override the default full naming of the application                                                                       | `""`  |
| `nodeSelector`     | is a dictionary that adds scheduling constraints to the pods of the application. These constraints can specify the properties of the desired host of the pods. | `{}`  |
| `tolerations`      | A list of tolerations that can be applied to Pods of the application. Each entry should represent a separate toleration.                                       | `[]`  |
| `affinity`         | The `affinity` parameter is an object that sets the scheduling constraints on the pods of the application by setting affinity rules. Affinity rules            | `{}`  |
| `envVars`          | is a dictionary object that can be used to set Environment Variables in the application.                                                                       | `{}`  |
| `podAnnotations`   | provides additional metadata for pods in the form of annotations.                                                                                              | `{}`  |

### OCI parameters

| Name                          | Description                                                                                   | Value          |
| ----------------------------- | --------------------------------------------------------------------------------------------- | -------------- |
| `image.repository`            | denotes the name of the image repository, in this case 'nginx'                                | `nginx`        |
| `image.pullPolicy`            | a Kubernetes directive which describes how to fetch the Docker image                          | `IfNotPresent` |
| `image.credentials.name:`     | A string representing the name of these credentials, which can be any string.                 |                |
| `image.credentials.registry:` | The string value representing the domain of the image registry. In this example, "gitlab.com" |                |
| `image.credentials.username:` | The string value representing the username for authentication at the mentioned registry       |                |
| `image.credentials.password:` | The string value representing the password for the mentioned username at the registry         |                |
| `image.tag`                   | used to reference a specific image snapshot in the image repository.                          | `""`           |

### Service parameters

This section defines parameters for the service which exposes the application to the network.

| Name                  | Description                                                                                                   | Value       |
| --------------------- | ------------------------------------------------------------------------------------------------------------- | ----------- |
| `service.type`        | The type of the service. It can be one of the following: ClusterIP, NodePort, LoadBalancer, or ExternalName.  | `ClusterIP` |
| `service.port`        | The port on which the service will listen. Here, it is set to 80.                                             | `80`        |
| `service.targetPort`  | The port of the target Pods where the HTTP service is running. Here, it's also set to 80.                     | `80`        |
| `service.annotations` | This is the place to specify additional annotations for the service. Here, it's an empty object,              | `{}`        |

### Probe parameters

This section defines liveness, readiness, and startup probes for Kubernetes.
Probes allow Kubernetes to check for certain conditions and respond accordingly.

| Name               | Description                                                                                     | Value |
| ------------------ | ----------------------------------------------------------------------------------------------- | ----- |
| `probes.liveness`  | The kubelet uses liveness probes to know when to restart a container.                           |       |
| `probes.readiness` | The kubelet uses readiness probes to know when a container is ready to start accepting traffic. | `nil` |
| `probes.startup`   | The kubelet uses startup probes to know when a container application has started.               |       |

### Jobs parameters

| Name                    | Description                                                               | Value |
| ----------------------- | ------------------------------------------------------------------------- | ----- |
| `jobs.cron`             | This is a list of cron jobs that are scheduled to run at specific times.  | `[]`  |
| `jobs.hooks.preUpgrade` | This is a list of hooks that run prior to upgrading the application.      |       |

### Ingress parameters

'ingress' controls the properties of the Ingress resource to be created for the application.

| Name                               | Description                                                                                       | Value              |
| ---------------------------------- | ------------------------------------------------------------------------------------------------- | ------------------ |
| `ingress.className`                | Optional string used to tie Ingress resources with specific IngressClass objects.                 | `nil`              |
| `ingress.enabled`                  | Flag to enable the creation of the Ingress. Default is 'false'.                                   | `false`            |
| `ingress.certManagerClusterIssuer` | The name of the certificate issuer object to be used by CertManager for issuing TLS certificates. | `letsencrypt-prod` |
| `ingress.annotations`              | Further annotations to add to the Ingress resource object.                                        | `{}`               |
| `ingress.hosts`                    | A list of hosts and their respective paths to be included in the Ingress object.                  | `[]`               |
| `ingress.tls.additional`           | Additional configuration for ingresses that will enable SSL certificates for multiple hosts.      | `[]`               |

### Resource parameters

This configuration block defines the resources requests and limits for the pods in the deployment.
Kubernetes uses requests for scheduling pods on its nodes, and to ensure fair resource sharing between pods.
Limits come into play to guarantee a maximum amount of a resource that a pod can consume, preventing it affecting other pods adversely.
This block has two main components: `limits` and `requests`, each for CPU and memory.

| Name                         | Description                                                                                                                                            | Value |
| ---------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ | ----- |
| `resources.limits.cpu:`      | The maximum amount of CPU a pod can use. If the CPU use exceeds this limit,                                                                            |       |
| `resources.limits.memory:`   | The maximum amount of memory a pod can use. If a pod exceeds this limit,                                                                               |       |
| `resources.requests.cpu:`    | The amount of CPU a pod requests upon scheduling. Kubernetes guarantees to allocate this amount of CPU. '100m' implies 0.1 of a CPU core.              |       |
| `resources.requests.memory:` | The amount of memory a pod requests upon scheduling. Kubernetes guarantees to allocate this amount of memory. '128Mi' implies 128 Mebibytes of memory. |       |

### Autoscaling

Parameters for enabling and controlling autoscaling of pods based on defined conditions.
note: these settings only apply when autoscaling is enabled.

| Name                                            | Description                                                                                                                                     | Value   |
| ----------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------- | ------- |
| `autoscaling.enabled`                           | Set this to true to enable autoscaling, and to false to disable it.                                                                             | `false` |
| `autoscaling.minReplicas`                       | Defines the minimum number of pods to deploy.                                                                                                   | `1`     |
| `autoscaling.maxReplicas`                       | Defines the maximum number of pods which can be deployed.                                                                                       | `100`   |
| `autoscaling.targetCPUUtilizationPercentage`    | Defines the percentage of CPU utilization that the system should attempt to maintain. When this value is exceeded, the system will scale up.    | `80`    |
| `autoscaling.targetMemoryUtilizationPercentage` | Defines the percentage of memory utilization that the system should attempt to maintain. When this value is exceeded, the system will scale up. |         |

### Service Account

This configuration block is used for controlling parameters linked to service account preferences.
The service account is used by the application in the code to interact with the Kubernetes API.

| Name                         | Description                                                                                                                           | Value   |
| ---------------------------- | ------------------------------------------------------------------------------------------------------------------------------------- | ------- |
| `serviceAccount.create`      | A boolean field indicating whether a new service account should be created for your deployment (true) or use an existing one (false). | `false` |
| `serviceAccount.annotations` | An empty dictionary to add any additional annotations you may want associated with the service account.                               | `{}`    |
| `serviceAccount.name`        | The name for the serviceAccount to be created. If not defined, a name is generated using the `fullname` template.                     | `""`    |

### Persistence parameters

In this section, you can specify where the application will persist data. For this application, all persistent data is stored on a volume.

| Name                                  | Description                                                                                                                                                                                                                                                                              | Value |
| ------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----- |
| `persistence.[volume-name].size`      | Indicates the size of the volume capacity for data persistent storage. It's string type. The format follows the pattern of the kubernetes resource model (integer number + units like Ki, Mi, Gi, Ti, Pi, Ei). In this example, the size is '8Gi' which denotes a volume of 8 Gibibytes. |       |
| `persistence.[volume-name].mountPath` | A string that specifies the path within the container at which the volume should be mounted. In this snippet, '/data' is the directory path inside the container where the volume is mounted.                                                                                            |       |
