{{/* vim: set filetype=mustache: */}}
{{- define "camelToDash" -}}
{{- regexReplaceAll "([a-z0-9])([A-Z])" . "${1}-${2}" | lower -}}
{{- end -}}
